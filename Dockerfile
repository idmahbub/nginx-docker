FROM nginx

LABEL maintainer="NGINX Docker Maintainers <docker-maint@nginx.com>"
RUN apt-get update && apt-get install -y \
    git \
    geoip-database \
    libgeoip1
COPY ./data/ /etc/nginx/
VOLUME /etc/nginx
WORKDIR /code